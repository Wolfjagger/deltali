module deltali.shape.curve;

public import std.typecons : Tuple;
public import deltali.tensor : Vec2F;

import std.container : Array, make;
import std.math : abs;
import std.typecons : isTuple;



enum Differentiable { None, Tangents, TanAndNorm }
enum BoundaryTopo { Open, Closed }



//TODO: Make this appendable.
interface ICurve(BoundaryTopo boundaryTopo, Differentiable differentiable) {

public:

    @property {

        Array!Vec2F.ConstRange pts() const;
        uint numPts() const;

        static if(boundaryTopo == BoundaryTopo.Open) {
            Vec2F start() const;
            Vec2F end() const;
        }

        static if(differentiable != Differentiable.None) {
            Array!Vec2F.ConstRange tangents() const;
            Array!Vec2F.ConstRange ptTangents() const;
            static if(differentiable == Differentiable.TanAndNorm)
                Array!Vec2F.ConstRange normals() const;
        }

        static if(boundaryTopo == BoundaryTopo.Open) {
            float endptDist() const;
        }
        float arclength() const;

    }

    bool intersects(Vec2F start, Vec2F end) const;
    Array!Vec2F intersectionPts(Vec2F start, Vec2F end) const;

/*    static if(boundaryTopo == BoundaryTopo.Closed) {
        bool contains(Vec2F pt) const;
    }*/

}



class Curve(BoundaryTopo boundaryTopo, Differentiable differentiable,
            alias CurveData)
: ICurve!(boundaryTopo, differentiable) {

    static assert(isTuple!CurveData);

    // Locations of pts on curve
    protected {

        CurveData curveData;

        Array!Vec2F m_pts;
        static if(differentiable != Differentiable.None) {
            Array!Vec2F m_tangents;
            Array!Vec2F m_ptTangents;
            static if(differentiable == Differentiable.TanAndNorm)
                Array!Vec2F m_normals;
        }

    }

    static if(boundaryTopo == BoundaryTopo.Open) {
        private const uint minNumPts = 2;
    } else {
        private const uint minNumPts = 3;
    }

    @property {
        
        public final override Array!Vec2F.ConstRange pts() const {
            return m_pts[];
        }
        public final override uint numPts() const {
            return m_pts.length;
        }

        static if(boundaryTopo == BoundaryTopo.Open) {
            public final override Vec2F start() const {
                return m_pts.front;
            }
            public final override Vec2F end() const {
                return m_pts.back;
            }
        }

        static if(differentiable != Differentiable.None) {
            public final override Array!Vec2F.ConstRange tangents() const {
                return cast(Array!Vec2F.ConstRange)m_tangents[];
            }
            public final override Array!Vec2F.ConstRange ptTangents() const {
                return cast(Array!Vec2F.ConstRange)m_ptTangents[];
            }
            static if(differentiable == Differentiable.TanAndNorm) {
                public final override Array!Vec2F.ConstRange normals() const {
                    return cast(Array!Vec2F.ConstRange)m_normals[];
                }
            }
        }

    }


    
    static if(boundaryTopo == BoundaryTopo.Open) {
        private float m_endptDist;
        @property public final override float endptDist() const {
            return m_endptDist;
        }
    }

    private float m_arclength;
    @property public final override float arclength() const {
        return m_arclength;
    }



public:

    this(CurveData init)
    out {
        assert(numPts >= minNumPts);
    } body {

        curveData = init;

        m_pts = make!(Array!Vec2F)();

        doInit();
        postMutation();

    }



    void update(CurveData data) {
        curveData = data;
        doUpdate();
        postMutation();
    }



    bool intersects(Vec2F start, Vec2F end) const {
        
        auto diff = end - start;
        if(diff.normSqu() < 0.001) return false;

        for(auto i=1; i < m_pts.length; ++i) {
            auto prevPt = m_pts[i-1];
            auto pt = m_pts[i];

            auto diffCurve = pt - prevPt;
            auto diffCross = diff.cross(diffCurve);

            if(abs(diffCross) >= 0.001) {
                auto diffBase = start - prevPt;
                auto t = diffCurve.cross(diffBase)/diffCross;
                if(t >= 0 && t <= 1) return true;
            }

        }

        static if(boundaryTopo == BoundaryTopo.Closed) {

            auto diffCurve = m_pts[0] - m_pts[$-1];
            auto diffCross = diff.cross(diffCurve);

            if(abs(diffCross) >= 0.001) {
                auto diffBase = start - m_pts[$-1];
                auto t = diffCurve.cross(diffBase)/diffCross;
                if(t >= 0 && t <= 1) return true;
            }

        }

        return false;

    }

    Array!Vec2F intersectionPts(Vec2F start, Vec2F end) const {

        Array!Vec2F intPts;

        auto diff = end - start;
        if(diff.normSqu() < 0.001) return intPts;

        for(auto i=1; i < m_pts.length; ++i) {
            auto prevPt = m_pts[i-1];
            auto pt = m_pts[i];

            auto diffCurve = pt - prevPt;
            auto diffCross = diff.cross(diffCurve);

            if(abs(diffCross) >= 0.001) {
                auto diffBase = start - prevPt;
                auto t = diffCurve.cross(diffBase)/diffCross;
                if(t >= 0 && t <= 1) intPts ~= prevPt + t * diffCurve;
            }

        }

        static if(boundaryTopo == BoundaryTopo.Closed) {

            auto diffCurve = m_pts[0] - m_pts[$-1];
            auto diffCross = diff.cross(diffCurve);

            if(abs(diffCross) >= 0.001) {
                auto diffBase = start - m_pts[$-1];
                auto t = diffCurve.cross(diffBase)/diffCross;
                if(t >= 0 && t <= 1) intPts ~= m_pts[$-1] + t * diffCurve;
            }

        }

        return intPts;

    }



protected:

    void postMutation() {

        static if(boundaryTopo == BoundaryTopo.Open)
            m_endptDist = calcEndptDist();

        static if(differentiable != Differentiable.None) {
            calcTangents();
            calcPtTangents();
            static if(differentiable == Differentiable.TanAndNorm) calcNormals();
        }

        calcArclength();

    }



private:

    static if(boundaryTopo == BoundaryTopo.Open) {
        float calcEndptDist() const {
            return start.dist(end);
        }
    }



    static if(differentiable != Differentiable.None) {

        void calcTangents() {

            static if(boundaryTopo == BoundaryTopo.Open) {
                m_tangents.length = numPts-1;
            } else {
                m_tangents.length = numPts;
            }

            for(uint idx=0; idx<numPts-1; ++idx) {
                m_tangents[idx] = m_pts[idx+1]-m_pts[idx];
            }

            static if(boundaryTopo == BoundaryTopo.Closed)
                m_tangents[numPts-1] = m_pts[0] - m_pts[numPts-1];

            foreach(ref tangent; m_tangents) tangent.toUnit();

        }

        void calcPtTangents() {

            m_ptTangents.length = numPts;

            static if(boundaryTopo == BoundaryTopo.Open) {
                m_ptTangents[0] = m_tangents[0];
            } else {
                m_ptTangents[0]
                    = (m_tangents[0] + m_tangents[numPts-1])/2;
            }

            for(auto idx=1; idx<numPts-1; ++idx) {
                m_ptTangents[idx]
                    = (m_tangents[idx] + m_tangents[idx-1])/2;
            }

            static if(boundaryTopo == BoundaryTopo.Open) {
                m_ptTangents[numPts-1] = m_tangents[numPts-2];
            } else {
                m_ptTangents[numPts-1]
                    = (m_tangents[numPts-1] + m_tangents[numPts-2])/2;
            }

            foreach(ref tangent; m_ptTangents) tangent.toUnit();

        }

        static if(differentiable == Differentiable.TanAndNorm) {

            void calcNormals() {

                m_normals.length = numPts;

                static if(boundaryTopo == BoundaryTopo.Open) {
                    m_normals[0] = 0;
                } else {
                    auto deltax = m_pts[1].dist(m_pts[numPts-1])/2;
                    m_normals[0]
                        = (m_tangents[0] - m_tangents[numPts-1])/deltax;
                }
            
                for(auto idx=1; idx<numPts-1; ++idx) {
                    auto deltax = m_pts[idx+1].dist(m_pts[idx-1])/2;
                    m_normals[idx]
                        = (m_tangents[idx] - m_tangents[idx-1])/deltax;
                }

                static if(boundaryTopo == BoundaryTopo.Open) {
                    m_normals[numPts-1] = 0;
                } else {
                    auto deltax = m_pts[1].dist(m_pts[numPts-1])/2;
                    m_normals[numPts-1]
                        = (m_tangents[numPts-1] - m_tangents[numPts-2])/deltax;
                }

            }

        }

    }



protected:

    // Do not call without finishing doFillInterior/doShiftInterior
    float calcArclength() {

        auto length=0f;
        auto prevPt = m_pts[0];
        foreach(pt; m_pts[1..$-1]) {
            length += (pt-prevPt).norm();
            prevPt = pt;
        }

        static if(boundaryTopo == BoundaryTopo.Closed) {
            length += (m_pts[0]-m_pts[$-1]).norm();
        }

        return length;

    }

    abstract void doInit();

    abstract void doUpdate();

}
