module deltali.shape.line;

public import deltali.shape.curve;



alias LineData = Tuple!(Vec2F, "start", Vec2F, "end");

class Line(Differentiable differentiable)
: Curve!(BoundaryTopo.Open, differentiable, LineData) {

public:

    this(Vec2F startVec, Vec2F endVec) {

        LineData init;
        init.start = startVec;
        init.end = endVec;
        super(init);

    }



    @property {

        public void start(Vec2F newVec) {
            m_pts.front = newVec;
            postMutation();
        }
        public void end(Vec2F newVec) {
            m_pts.back = newVec;
            postMutation();
        }

    }



protected:

    final override float calcArclength() {
        return super.curveData.start.dist(super.curveData.end);
    }

    final override void doInit() {
        m_pts ~= curveData.start;
        m_pts ~= curveData.end;
    }

    final override void doUpdate() {
        m_pts[0] = curveData.start;
        m_pts[1] = curveData.end;
    }

}
