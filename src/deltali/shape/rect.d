module deltali.shape.rect;

public import deltali.shape.curve;



alias RectData = Tuple!(Vec2F, "center", float, "width", float, "height");


class Rect(Differentiable differentiable)
: Curve!(BoundaryTopo.Closed, differentiable, RectData) {

public:

    this(Vec2F center, float width, float height) {

        RectData init;
        init.center = center;
        init.width = width;
        init.height = height;
        super(init);

    }



protected:

    final override float calcArclength() {
        return 2*(curveData.width + curveData.height);
    }

    final override void doInit() {
        auto vecDiff1 = Vec2F(curveData.width/2, curveData.height/2);
        auto vecDiff2 = Vec2F(curveData.width/2, -curveData.height/2);
        m_pts ~= curveData.center - vecDiff1;
        m_pts ~= curveData.center - vecDiff2;
        m_pts ~= curveData.center + vecDiff1;
        m_pts ~= curveData.center + vecDiff2;
    }

    final override void doUpdate() {
        auto vecDiff1 = Vec2F(curveData.width/2, curveData.height/2);
        auto vecDiff2 = Vec2F(curveData.width/2, -curveData.height/2);
        m_pts[0] = curveData.center - vecDiff1;
        m_pts[1] = curveData.center - vecDiff2;
        m_pts[2] = curveData.center + vecDiff1;
        m_pts[3] = curveData.center + vecDiff2;
    }



    invariant {
        assert(curveData.width > 0);
        assert(curveData.height > 0);
    }

}
