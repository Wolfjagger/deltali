module deltali.shape.roundrect;

public import deltali.shape.curve;

import std.math : abs, cos, PI_2;
import std.container.array : Array;
import std.algorithm.mutation : reverse;


enum CornerMode {
    Stretched, Min, Max
}

alias RoundRectData = Tuple!(
    Vec2F, "center",
    float, "width",
    float, "height",
    float, "cornerRadius",
    uint, "cornerPtCount",
    CornerMode, "cornerMode",
    float, "resRatio",
    float function(float), "cornerFcn"
);


class RoundRect(Differentiable differentiable)
: Curve!(BoundaryTopo.Closed, differentiable, RoundRectData) {

public:

    this(Vec2F center, float width, float height,
         float cornerRadius, uint cornerPtCount = 5,
         CornerMode cornerMode = CornerMode.Stretched,
         float resRatio = 1,
         float function(float) cornerFcn = &cos) {

        RoundRectData init;
        init.center = center;
        init.width = width;
        init.height = height;
        init.cornerRadius = cornerRadius;
        init.cornerPtCount = cornerPtCount;
        init.cornerMode = cornerMode;
        init.resRatio = resRatio;
        init.cornerFcn = cornerFcn;
        super(init);

    }



protected:

    final override void doInit() {

        // Establish various lengths and positions
        auto centerVec = curveData.center;
        auto left = centerVec.x - curveData.width/2;
        auto right = centerVec.x + curveData.width/2;
        auto top = centerVec.y + curveData.height/2;
        auto bot = centerVec.y - curveData.height/2;

        auto numPt = curveData.cornerPtCount;

        // Generate corner pts, in accordance with m_cornerFcn
        Array!float cosPts;
        cosPts.reserve(numPt);
        for(auto i=0; i<numPt; ++i) {
            auto rads = PI_2 * (i+1)/(numPt+1);
            cosPts ~= curveData.cornerFcn(rads);
        }
        auto sinPts = cosPts.dup();
        reverse(sinPts[]);

        // Reset position list
        m_pts.reserve(8 + 4*curveData.cornerPtCount);

        // Extra weight, dependent on cornerMode.
        auto cornerX = curveData.cornerRadius;
        auto cornerY = curveData.cornerRadius;
        auto cornerMode = curveData.cornerMode;
        auto resRatio = curveData.resRatio;
        if(cornerMode != CornerMode.Stretched) {

            //TODO: Generalize this and other stretch vs. fixed ratio decisions
            if((cornerMode == CornerMode.Max && resRatio < 1) ||
               (cornerMode == CornerMode.Min && resRatio > 1)) {
                cornerY /= resRatio;
            } else {
                cornerX *= resRatio;
            }

        }

        // Define a vec for reuse as corner basing point
        Vec2F base;

        // TL to TR
        m_pts ~= Vec2F(left + cornerX, top);
        m_pts ~= Vec2F(right - cornerX, top);

        // TR corner
        base = Vec2F(right - cornerX, top - cornerY);
        for(auto i=0; i<numPt; ++i) {
            m_pts ~=
                base + Vec2F(cornerX*sinPts[i], cornerY*cosPts[i]);
        }

        // TR to BR
        m_pts ~= Vec2F(right, top - cornerY);
        m_pts ~= Vec2F(right, bot + cornerY);

        // BR corner
        base = Vec2F(right - cornerX, bot + cornerY);
        for(auto i=0; i<numPt; ++i) {
            m_pts ~=
                base + Vec2F(cornerX*cosPts[i], -cornerY*sinPts[i]);
        }

        // BR to BL
        m_pts ~= Vec2F(right - cornerX, bot);
        m_pts ~= Vec2F(left + cornerX, bot);

        // BL corner
        base = Vec2F(left + cornerX, bot + cornerY);
        for(auto i=0; i<numPt; ++i) {
            m_pts ~=
                base + Vec2F(-cornerX*sinPts[i], -cornerY*cosPts[i]);
        }

        // BL to TL
        m_pts ~= Vec2F(left, bot + cornerY);
        m_pts ~= Vec2F(left, top - cornerY);

        // TL corner
        base = Vec2F(left + cornerX, top - cornerY);
        for(auto i=0; i<numPt; ++i) {
            m_pts ~=
                base + Vec2F(-cornerX*cosPts[i], cornerY*sinPts[i]);
        }

    }

    final override void doUpdate() {

        // Establish various lengths and positions
        auto centerVec = curveData.center;
        auto left = centerVec.x - curveData.width/2;
        auto right = centerVec.x + curveData.width/2;
        auto top = centerVec.y + curveData.height/2;
        auto bot = centerVec.y - curveData.height/2;

        auto numPt = curveData.cornerPtCount;

        // Generate corner pts, in accordance with m_cornerFcn
        Array!float cosPts;
        cosPts.reserve(numPt);
        for(auto i=0; i<numPt; ++i) {
            auto rads = PI_2 * (i+1)/(numPt+1);
            cosPts ~= curveData.cornerFcn(rads);
        }
        auto sinPts = cosPts.dup();
        reverse(sinPts[]);

        // Reset position list
        m_pts.length = 8 + 4*curveData.cornerPtCount;

        // Extra weight, dependent on cornerMode.
        auto cornerX = curveData.cornerRadius;
        auto cornerY = curveData.cornerRadius;
        auto cornerMode = curveData.cornerMode;
        auto resRatio = curveData.resRatio;
        if(cornerMode != CornerMode.Stretched) {

            //TODO: Generalize this and other stretch vs. fixed ratio decisions
            if((cornerMode == CornerMode.Max && resRatio < 1) ||
               (cornerMode == CornerMode.Min && resRatio > 1)) {
                cornerY /= resRatio;
            } else {
                cornerX *= resRatio;
            }

        }

        // Define a vec for reuse as corner basing point
        Vec2F base;
        auto idxPt = 0;

        // TL to TR
        m_pts[idxPt] = Vec2F(left + cornerX, top);
        m_pts[idxPt] = Vec2F(right - cornerX, top);

        // TR corner
        base = Vec2F(right - cornerX, top - cornerY);
        for(auto i=0; i<numPt; ++i) {
            m_pts[idxPt] =
                base + Vec2F(cornerX*sinPts[i], cornerY*cosPts[i]);
        }

        // TR to BR
        m_pts[idxPt] = Vec2F(right, top - cornerY);
        m_pts[idxPt] = Vec2F(right, bot + cornerY);

        // BR corner
        base = Vec2F(right - cornerX, bot + cornerY);
        for(auto i=0; i<numPt; ++i) {
            m_pts[idxPt] =
                base + Vec2F(cornerX*cosPts[i], -cornerY*sinPts[i]);
        }

        // BR to BL
        m_pts[idxPt] = Vec2F(right - cornerX, bot);
        m_pts[idxPt] = Vec2F(left + cornerX, bot);

        // BL corner
        base = Vec2F(left + cornerX, bot + cornerY);
        for(auto i=0; i<numPt; ++i) {
            m_pts[idxPt] =
                base + Vec2F(-cornerX*sinPts[i], -cornerY*cosPts[i]);
        }

        // BL to TL
        m_pts[idxPt] = Vec2F(left, bot + cornerY);
        m_pts[idxPt] = Vec2F(left, top - cornerY);

        // TL corner
        base = Vec2F(left + cornerX, top - cornerY);
        for(auto i=0; i<numPt; ++i) {
            m_pts[idxPt] =
                base + Vec2F(-cornerX*cosPts[i], cornerY*sinPts[i]);
        }

    }



    invariant {

        assert(curveData.center.x > 0);
		assert(curveData.center.y > 0);
		assert(curveData.width > 0);
		assert(curveData.height > 0);
		assert(curveData.cornerRadius > 0);
        if(curveData.cornerMode == CornerMode.Stretched) {
            assert(curveData.cornerRadius < curveData.width/2);
            assert(curveData.cornerRadius < curveData.height/2);
        } else {
            if((curveData.cornerMode == CornerMode.Max
                && curveData.resRatio < 1) ||
               (curveData.cornerMode == CornerMode.Min
                && curveData.resRatio > 1)) {
                assert(curveData.cornerRadius < curveData.width/2);
                assert(curveData.cornerRadius <
                       curveData.height*curveData.resRatio/2);
            } else {
                assert(curveData.cornerRadius <
                       curveData.width/(2*curveData.resRatio));
                assert(curveData.cornerRadius < curveData.height/2);
            }
        }
        assert(abs(1-curveData.cornerFcn(0)) < 0.01
               && abs(curveData.cornerFcn(PI_2)) < 0.01);

    }

}
