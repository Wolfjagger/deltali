module deltali.tensor.vec;

import std.algorithm.iteration : each, sum;
import std.math : approxEqual, sqrt;



/// Vector class with templated data-type and dimension
template Vec(Data, uint dim) {

    static assert (dim > 0);

    @nogc @safe pure nothrow struct Vec {

        Data[dim] data = void;

        this(Data[dim] initData...)
        {
            data = initData;
        }

        auto ref opIndex(uint i) inout
        {
            return data[i];
        }


        static if(dim == 2 || dim == 3) {
            @property auto x() const { return data[0]; }
            @property auto x(Data value) { return data[0] = value; }
            @property auto y() const { return data[1]; }
            @property auto y(Data value) { return data[1] = value; }
        }
         
        static if(dim == 2) {
            /// Cross product
            auto cross(VecOther : Vec!(OtherData, dim), OtherData)
                (VecOther other) const
            {
                return data[0]*other[1] - data[1]*other[0];
            }
        }

        static if(dim == 3) {
            @property auto z() { return data[2]; }
            @property auto z(Data value) { return data[2] = value; }

            /// Cross product
            auto cross(VecOther : Vec!(OtherData, dim), OtherData)
                (VecOther other) const
            {
                alias NewData = typeof(Data(0)*OtherData(0));
                Vec!(NewData, dim) result;
                result[0] = data[1]*other[2] - data[2]*other[1];
                result[1] = data[2]*other[0] - data[0]*other[2];
                result[2] = data[0]*other[1] - data[1]*other[0];
                return result;
            }

        }



        // Scalar operations on Vec
        auto ref opOpAssign(string op, T)(T other)
            if(__traits(isArithmetic, T))
        {
            mixin("data[] "~op~"= other;");
            return this;
        }
        auto opUnary(string op)() const
        {
            Data[dim] newData;
            newData = mixin(op~"data[]");
            return Vec(newData);
        }
        auto opBinary(string op, T)(T other) const
            if(__traits(isArithmetic, T))
        {
            alias NewData = typeof(Data(0)*T(0));
            NewData[dim] newData;
            newData[] = mixin("data[] "~op~" other");
            return Vec!(NewData, dim)(newData);
        }
        auto opBinaryRight(string op, T)(T other) const
            if(__traits(isArithmetic, T))
        {
            alias NewData = typeof(Data(0)*T(0));
            NewData[dim] newData;
            newData[] = mixin("other "~op~" data[]");
            return Vec!(NewData, dim)(newData);
        }

        // Vector parallel operations
        auto ref opOpAssign(string op, VecOther : Vec!(OtherData, dim), OtherData)
            (VecOther other)
        {
            mixin("data[] "~op~"= other.data[];");
            return this;
        }

        auto opBinary(string op, VecOther : Vec!(OtherData, dim), OtherData)
            (VecOther other) const
        {
            alias NewData = typeof(mixin("Data(0)"~op~"OtherData(0)"));
            NewData[dim] newData;
            newData[] = mixin("data[] "~op~" other.data[]");
            return Vec!(NewData, dim)(newData);
        }

        /// Dot product
        auto dot(VecOther : Vec!(OtherData, dim), OtherData)
            (VecOther other) const
        {
            alias NewData = typeof(Data(0)*OtherData(0));
            NewData[dim] newData;
            newData[] = data[]*other.data[];
            return sum(newData[]);
        }

        /// Normalization squ
        auto normSqu() const
        {
            auto sum = Data(0);
            foreach(Data value; data) {
                sum += value*value;
            }
            return sum;
        }
        auto distSqu(VecOther : Vec!(OtherData, dim), OtherData)
            (VecOther other) const
        {
            alias NewData = typeof(Data(0)-OtherData(0));
            NewData[dim] newData;
            newData[] = data[] - other.data[];
            newData.each!(d => d*d);
            return sum(newData[]);
        }

        /// Magnitude
        static if(__traits(isFloating, Data)) {
            auto norm() const {
                return sqrt(normSqu());
            }
            auto dist(VecOther : Vec!(OtherData, dim), OtherData)
                (VecOther other) const {
                return sqrt(distSqu(other));
            }
            auto unitVec() const {
                return this / norm();
            }
            auto toUnit() {
                this = unitVec();
            }
        }

    }

}

// Add cross product to dim=3 specialization. Possibly with template mixin.

alias VecF(uint dim) = Vec!(float, dim);
alias Vec2F = VecF!2;
alias Vec3F = VecF!3;

alias VecI(uint dim) = Vec!(int, dim);
alias Vec2I = VecI!2;
alias Vec3I = VecI!3;



unittest {

    VecF!(3) vecF1;

    assert(approxEqual(vecF1[0], 0) &&
           approxEqual(vecF1[1], 0) &&
           approxEqual(vecF1[2], 0));

    float f0=1, f1=2, f2=3, f3=4.5, f4=5.5, f5=6.5;
    int i0=7, i1=8, i2=9;
    vecF1 = VecF!(3)(f0, f1, f2);
    auto vecF2 = Vec3F(f3, f4, f5);
    auto vecI = VecI!(3)(i0, i1, i2);
    assert(approxEqual(vecF1.dot(vecF2), f0*f3 + f1*f4 + f2*f5));
    assert(approxEqual(vecF1.dot(vecI), f0*i0 + f1*i1 + f2*i2));
    assert(approxEqual(vecF1.normSqu(), f0*f0 + f1*f1 + f2*f2));
    assert(approxEqual(vecF1.norm(), sqrt(f0*f0 + f1*f1 + f2*f2)));

    auto vecAdd = vecF1 + vecF2;
    assert(approxEqual(vecAdd[0], f0+f3) &&
           approxEqual(vecAdd[1], f1+f4) &&
           approxEqual(vecAdd[2], f2+f5));

    auto vecMult = vecF1 * vecI;
    assert(approxEqual(vecMult[0], f0*i0) &&
           approxEqual(vecMult[1], f1*i1) &&
           approxEqual(vecMult[2], f2*i2));

    auto vecF3 = vecF1;
    vecF3 -= vecF2;
    assert(approxEqual(vecF3[0], f0-f3) &&
           approxEqual(vecF3[1], f1-f4) &&
           approxEqual(vecF3[2], f2-f5));

    auto cross = vecF1.cross(vecF2);
    assert(approxEqual(cross.dot(vecF1), 0) &&
           approxEqual(cross.dot(vecF2), 0));
    assert(approxEqual(cross[0], f1*f5-f2*f4) &&
           approxEqual(cross[1], f2*f3-f0*f5) &&
           approxEqual(cross[2], f0*f4-f1*f3));

}
