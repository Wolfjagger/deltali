module deltali.util.common_types;



enum LR { Left, Right }
enum UD { Up, Down }

enum Compass {
    Up, Down, Left, Right
}
